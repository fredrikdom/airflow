#include <iostream>

#include "lib/multizone.h"
//#include "lib/leakage.h"

int main()
{
  MultiZone b;
	
  b.addFixedPressure(-2.0);    // #0
  b.addFixedPressure(4.0);    // #1

  b.addZone(3.0, 273);      // #0
  //b.addZone(3.0, 293);      // #1
	
  //b.addFixedPressure(0);   // #2
	
  //b.addFixedPressure(0);    // #0
  //b.addFixedPressure(2);    // #0
	
  //b.addLeakage(3.0, 0.00474, 0.65, {0, 0}, {ZONE, BOUNDARY}); // Zone-connection
  //b.addLeakage(2.0, 0.00474, 0.65, {0, 1}, {ZONE, BOUNDARY}); // Zone-connection
  b.addLeakage(2.0, 0.00236, 0.65, {0, 0}, {ZONE, BOUNDARY}); // Zone-connection
  b.addLeakage(2.0, 0.00236, 0.65, {0, 1}, {ZONE, BOUNDARY}); // Zone-connection
  //b.addLeakage(3.0, 1.0, 0.65, {0, 1}, {ZONE, ZONE}); // Boundary
  //b.addLeakage(3.0, 1.0, 0.65, {0, 1}, {ZONE, ZONE}); // Boundary

  //b.removeLeakage(3);
  //b.solve();

  //b.addLeakage(2.0, 1.0, 0.65, {0, 0}, {BOUNDARY, ZONE}); // Boundary
  //b.addLeakage(1.5, 1.0, 0.65, {0, 1}, {ZONE, ZONE}); // Boundary
 
  /* 
  b.addLeakage(0.0, 1.0, 0.65, 0, 1); // Zone-connection
  b.addLeakage(3.0, 1.0, 0.65, 0, 2); // Boundary
  b.addLeakage(2.0, 1.0, 0.65, 1, 2); // Boundary
  */

  //b.addLeakage(0.0, 2.0, 0.65, 0, 1);
  //b.adjustZoneTemperature(5, 0);

	//b.addMassFlow(0.02, 0);
	//b.addMassFlow(-0.048, 0);
	
	double exhaust = -0.012 + 0.0012*8;
	b.addMassFlow(exhaust, 0);

  b.solve();
  
  cout << endl;
  cout << "Leakage 0: " << b.getLeakageMassflow(0) << endl;
  cout << "Leakage 1: " << b.getLeakageMassflow(1) << endl;
  cout << "Fan: " << exhaust << endl;
  cout << "Total: " << b.getLeakageMassflow(0) + b.getLeakageMassflow(1) + exhaust << endl;
  //cout << b.getLeakageMassflow(1) << endl;
  //cout << b.getLeakageMassflow(2) << endl;
  
  cout << "Zone T: " << b.getZoneTemperature(0) << endl;

  cout << "Max: " << b.getMaxLeakageMassFlow() << endl;

  cout << "Pressure at 3 m height, zone 0: " << b.getPressure(0, 3.0) << endl;
  cout << "Pressure at 0 m height, zone 0: " << b.getPressure(0, 0.0) << endl;

	for (int i=0; i<21; i++)
	{
		//double exhaust = 0.02 + 0.0001*i - 0.048;
		double exhaust = -0.012 + 0.0012*i;
		b.addMassFlow(exhaust, 0);
		b.solve();

    cout << b.getPressure(0, 2.0) << " " << -0.012 + 0.0012*i << " " << b.getLeakageMassflow(0) << " " << b.getLeakageMassflow(1) << endl;

	}

  return 0;
}
