#include <iostream>

#include "lib/multizone.h"
//#include "lib/leakage.h"


// Used to test convergence criteria
//
//


int main()
{
  MultiZone b;
	
  b.addFixedPressure(0.0); // #0

  b.addZone(3.0, 273);      // #0
  b.addZone(3.0, 293);      // #1
	
  b.addLeakage(1.0, 1.0, 0.65, {0, 1}, {ZONE, ZONE}); // Zone-zone

  b.addLeakage(0.0, 1.0, 0.65, {0, 0}, {ZONE, BOUNDARY}); // Zone-connection
  b.addLeakage(2.0, 1.0, 0.65, {1, 0}, {ZONE, BOUNDARY}); // Zone-connection


  b.addLeakage(0.0, 1.0, 0.65, {0, 0}, {ZONE, BOUNDARY}); // Zone-connection
  b.addLeakage(0.0, 1.0, 0.65, {0, 0}, {ZONE, BOUNDARY}); // Zone-connection
  b.addLeakage(0.0, 1.0, 0.65, {0, 0}, {ZONE, BOUNDARY}); // Zone-connection

  for (int i=1; i<3; i++)
  {
    b.solve();
  
    cout << endl;
    cout << "Leakage 0: " << b.getLeakageMassflow(0) << endl;
    cout << "Leakage 1: " << b.getLeakageMassflow(1) << endl;
    cout << "Total: " << b.getLeakageMassflow(0) + b.getLeakageMassflow(1) << endl;

    cout << "Pressure at 3 m height, zone 0: " << b.getPressure(0, 3.0) << endl;
    cout << "Pressure at 0 m height, zone 0: " << b.getPressure(0, 0.0) << endl;

  }

  return 0;
}
