main: main.o multizone.o leakage.o
	g++ -std=c++11 -o main main.o multizone.o leakage.o -I/usr/local/include/eigen3
	
main.o: main.cpp
	g++ -std=c++11 -c main.cpp -I/usr/local/include/eigen3
	
multizone.o: lib/multizone.cpp lib/multizone.h
	g++ -std=c++11 -c lib/multizone.cpp -I/usr/local/include/eigen3
	
leakage.o: lib/leakage.cpp lib/leakage.o
	g++ -std=c++11 -c lib/leakage.cpp

all: main ;

.PHONY: clean

clean:
	# -f so this will succeed even if the files don't exist
	rm -f main main.o multizone multizone.o leakage leakage.o
