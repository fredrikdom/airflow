#ifndef MULTIZONE_H
#define MULTIZONE_H

// Included dependencies
#include <vector>
#include <iostream>

// Eigen, OS dependent path
  #ifdef _WIN32 // Windows
  #include <Eigen/Dense> 

  #elif __linux
  #include <Eigen/Dense>

  #elif __APPLE__
  #include <Eigen/Dense>

  #endif
// ------ //

//
#include "leakage.h"

using namespace std;
using namespace Eigen;

struct Zone
{
	double P;

	double h;
	double T;

	double m; // Prescribed mass flow to zone

	int id;
};

struct Boundary
{
	double P;
	//int id;
};

struct Node
{
	double P;
	char type;
  int i;
};

class MultiZone
{
private:

  double Te;
  vector<Leakage> leakages;
  vector<Zone> zones;
  vector<Boundary> boundaries;
	
  double sumMassFlow(int, bool);
  double sumAbsMassFlow(int, bool);
  double sumPartialDerivative(int, int, bool);
  double stackEffect(double T, double h);
  void calcStackEffect();
  void update(); 
  bool solver(bool lin);

public:

	MultiZone();
	
  void addLeakage(double, double, double, vector<int>, vector<NodeType>);
	
  void removeLeakage(int);
  
  void addZone(double, double);

  void removeZone(int);
	
  void addFixedPressure(double);

	void addMassFlow(double massFlow, int zoneNumber);
	
  void solve();

  double getLeakageMassflow(int i);

  void setZoneTemperature(double T_, int i);

  void adjustZoneTemperature(double T_, int i);

  double getZoneTemperature(int i);

  double getMaxLeakageMassFlow();

  double getPressure(int zone_nr, double height);
};

#endif
