#include "multizone.h"

MultiZone::MultiZone()
{
	Te = 273;
}

void MultiZone::addLeakage(double h, double C, double n, 
  vector<int> con, vector<NodeType> types)
{
  // Postive flow direction is defined from lower node index
  // to higher node index/*
  
  leakages.push_back(Leakage(h, C, n, con, types)); 
}

void MultiZone::removeLeakage(int leakageNumber)
{
  // Removes the leakage if it exists

  int n_leakages = leakages.size();  
  
  if (n_leakages > leakageNumber)
  {
    leakages.erase(leakages.begin()+leakageNumber);
  }
}

void MultiZone::addZone(double h_, double T_)
{
	zones.push_back(Zone());
	zones.back().h = h_;
	zones.back().T = T_;
	zones.back().P = 0.0;
}

void MultiZone::removeZone(int zoneNumber)
{
  // Removes the zone if it exists
}

void MultiZone::addFixedPressure(double pressure)
		// change name to "boundary preesure..."
{
	boundaries.push_back(Boundary());
	boundaries.back().P = pressure;
}

void MultiZone::addMassFlow(double massFlow, int zoneNumber)
{
	//
	zones[zoneNumber].m = -massFlow; // change sign, why?
}

void MultiZone::update()
{
	// Update pressures	
	for (int i=0; i<leakages.size(); i++)
	{
	    //leakages[i].P1 = nodes[leakages[i].n1].P + leakages[i].Pstack1;
		//leakages[i].P2 = nodes[leakages[i].n2].P + leakages[i].Pstack2;

    // Update 1st node
    if (leakages[i].t1 == ZONE)
    {
      leakages[i].P1 = zones[leakages[i].n1].P + leakages[i].Pstack1;
    }
    else if (leakages[i].t1 == BOUNDARY)
    {
      leakages[i].P1 = boundaries[leakages[i].n1].P + leakages[i].Pstack1;
    }
    // Update 2nd node
    if (leakages[i].t2 == ZONE)
    {
      leakages[i].P2 = zones[leakages[i].n2].P + leakages[i].Pstack2;
    }
    else if (leakages[i].t2 == BOUNDARY)
    {
      leakages[i].P2 = boundaries[leakages[i].n2].P + leakages[i].Pstack2;
    }
	}
}

double MultiZone::stackEffect(double T, double h)
{
  // Calculates Stack Effect for given temperature and height

  double Patm = 101325.0;
  double R = 287.058;
  double g = 9.82;
  
  return -(Patm / (R * T)) * g * (1.0 - h);
}

void MultiZone::calcStackEffect()
{
  for (int i=0; i<leakages.size(); i++)
  {
    if (leakages[i].t1 == BOUNDARY)
    {
      leakages[i].T1 = Te;
      leakages[i].T2 = zones[leakages[i].n2].T;
    }
    else if (leakages[i].t2 == BOUNDARY)
    {
      leakages[i].T2 = Te;
      leakages[i].T1 = zones[leakages[i].n1].T;
    }
    else
    {
      leakages[i].T1 = zones[leakages[i].n1].T;
      leakages[i].T2 = zones[leakages[i].n2].T;
    }
  }
          
  for (int i=0; i<leakages.size(); i++)
  {
    leakages[i].Pstack1 = stackEffect(leakages[i].T1, leakages[i].h);
    leakages[i].Pstack2 = stackEffect(leakages[i].T2, leakages[i].h);
  }
}

double MultiZone::sumMassFlow(int zone, bool lin)
{
	// // // // // // // // // // // // //
	//
	//  Calculates the total massflow to
	//  zone from all zones and boundaries
	//
	// // // // // // // // // // // // //

	double m = 0.0;

	for (int i=0; i<leakages.size(); i++)
	{
		// Calculate massflow to zone
		if (leakages[i].n1 == zone && leakages[i].t1 == ZONE)
		{
			m -= leakages[i].massflow(lin); // was (-)
		}
		else if (leakages[i].n2 == zone && leakages[i].t2 == ZONE)
		{
			m += leakages[i].massflow(lin);
		}
	}

	m += zones[ZONE].m; // add prescribed massflow

	return m;
}

double MultiZone::sumAbsMassFlow(int zone, bool lin)
{
  double m = 0.0;

  for (int i=0; i<leakages.size(); i++)
  {
    if ((leakages[i].n1 == zone && leakages[i].t1 == ZONE)
          || (leakages[i].n2 == zone && leakages[i].t2 == ZONE))
    {
      m += abs(leakages[i].massflow(lin));
    }
  }

  // Absolute value of prescribed massflow
  if (zones[ZONE].m > 0)
  {
	  m += zones[ZONE].m; // add prescribed massflow
  }
  else
  {
	  m -= zones[ZONE].m; // add prescribed massflow
  }

  return m;
}

double MultiZone::sumPartialDerivative(int z1, int z2, bool lin)
{
	double pd = 0.0;

  // May contain redundant functions and expressions.... CHANGE THIS!

	for (int i=0; i<leakages.size(); i++)
	{
    // Boundaries
    if (leakages[i].t2 == BOUNDARY && leakages[i].n1 == z1)
    {
      pd -= leakages[i].partialDerivative(lin);
    }
    else if (leakages[i].t2 == BOUNDARY && leakages[i].n1 == z2)
    {
      pd += leakages[i].partialDerivative(lin);
    }

    else if (leakages[i].t1 == BOUNDARY && leakages[i].n2 == z1)
    {
      pd -= leakages[i].partialDerivative(lin); 
    }
    else if (leakages[i].t1 == BOUNDARY && leakages[i].n2 == z2)
    {
      pd += leakages[i].partialDerivative(lin); 
    }

    // Not a boundary
		else if (leakages[i].n1 == z1 && leakages[i].n2 == z2)
		{
		  pd += leakages[i].partialDerivative(lin);
	  }
    else if (leakages[i].n1 == z2 && leakages[i].n2 == z1)
    {
      pd += leakages[i].partialDerivative(lin);
    }

    // Between the same zone
		else if (leakages[i].n1 == z1 && z1 == z2)
		{
		  pd -= leakages[i].partialDerivative(lin);
		}
    else if (leakages[i].n2 == z1 && z1 == z2)
    {  
      pd -= leakages[i].partialDerivative(lin);
    }
	}

	return pd;
}

double MultiZone::getLeakageMassflow(int i)
{
  return leakages[i].massflow(false);
}

void MultiZone::setZoneTemperature(double T_, int i)
{
  if (i < zones.size())
  {
    zones[i].T = T_;
  }
  else
  {
    cout << " " << endl;
  }
}

void MultiZone::adjustZoneTemperature(double T_, int i)
{
  if (i < zones.size())
  {
    zones[i].T += T_;
  }
  else
  {
    cout << " " << endl;
  }
}

double MultiZone::getZoneTemperature(int i)
{
  if (i < zones.size())
  {
    return zones[i].T;
  }
  else
  {
    return 0;
  }
}

double MultiZone::getMaxLeakageMassFlow()
{
  // Return maximum massflow in all leakages

  double maxMassFlow = 0.0;
  for (int i=0; i<leakages.size(); i++)
  {
    if (abs(leakages[i].massflow(false)) > maxMassFlow)
    {
      maxMassFlow = abs(leakages[i].massflow(false));
    }
  }
  return maxMassFlow; 
}

double MultiZone::getPressure(int zone_nr, double height)
{
  // Takes zone nr and height as arguments and returns
  // the pressure at the specified height in the specified zone

  double pressure;

  if (zone_nr < 0) // Then outdoor pressure
  {
    //pressure = 101325.0;
    pressure = stackEffect(Te, height);
  }
  else
  {
    pressure = zones[zone_nr].P + stackEffect(zones[zone_nr].T, height);
  }

  return pressure;
}

bool MultiZone::solver(bool lin)
{
  bool runningSolver = true;
  bool hasConverged = false;

	int nZones = zones.size(); 
  int iIterations = 0;
	
	VectorXd B(nZones);
	B.setZero();

	MatrixXd J(nZones, nZones);
	J.setZero();

  while (runningSolver)
	{
		// Calculate mass flows to each zone and store in vector B
		for (int i=0; i<nZones; i++)
		{
			B(i) = sumMassFlow(i,lin); 
		}

		// Calculate Jacobian matrix J
		for (int i=0; i<nZones; i++)
		{
			for (int j=0; j<nZones; j++)
			{
				J(i,j) = sumPartialDerivative(i,j,lin);
			}
		}

    VectorXd P;
    P = J.colPivHouseholderQr().solve(B);

    // Update zone pressures
    for (int i=0; i<nZones; i++)
    {
      zones[i].P -= 0.75 * P(i);
    }

    update();
  
    // Convergence criteria
    double conv;
    for (int i=0; i<nZones; i++)
    {
      conv = abs(sumMassFlow(i,lin)) / sumAbsMassFlow(i,lin);
      if (conv < 0.00001)
      {
        // zone has converged
        hasConverged = true;

        // If every zone converges
        if (i == nZones - 1)
        {
            runningSolver = false;
        }
      }
      else
      {
        // zone did not converge
        hasConverged = false;
        break;
      }
    }

    // If it takes to long time to find a solution
    // variables are set to zero and solver is stopped
    iIterations++;
    if (iIterations > 1000)
    {
      hasConverged = false;
      runningSolver = false;

      if (lin)
      {
        cout << endl << "Failed to converge! (Linnear Initialization)" << endl;
      }
      else
      {
        cout << endl << "Failed to converge!" << endl;
      }

      // reset values to zero
      for (int i=0; i<leakages.size(); i++)
      {
        leakages[i].P1 = 0.0;
        leakages[i].P2 = 0.0;
      }
      for (int i=0; i<zones.size(); i++)
      {
        zones[i].P = 0.0;
      }
    }
  }
  return hasConverged;
}

void MultiZone::solve()
{
	calcStackEffect();
  update();

	int nZones = zones.size(); 
	
	VectorXd B(nZones);
	B.setZero();

	MatrixXd J(nZones, nZones);
	J.setZero();

	//bool hasConverged = false;
  //int iIterations = 0;
  //bool lin = true;

  if (solver(true)) // Start with linnear initialization, proceed if convergence
  {
    solver(false);
  }

  /*
  while (!hasConverged)
	{
    if (iIterations > 20)
    {
      lin = false;
    }

		// Calculate mass flows to each zone and store in vector B
		for (int i=0; i<nZones; i++)
		{
			B(i) = sumMassFlow(i,lin); 
		}

		// Calculate Jacobian matrix J
		for (int i=0; i<nZones; i++)
		{
			for (int j=0; j<nZones; j++)
			{
				J(i,j) = sumPartialDerivative(i,j,lin);
			}
		}
    //cout << "B: " << B << endl;
    //cout << "J: " << J << endl;

    VectorXd P;
    P = J.colPivHouseholderQr().solve(B);

    //cout << "P: " << P << endl << endl;
	  
    // Update zone pressures
    for (int i=0; i<nZones; i++)
    {
      zones[i].P -= 0.75 * P(i);
    }

    update();
  
    // Convergence criteria
    double conv;
    for (int i=0; i<nZones; i++)
    {
      conv = abs(sumMassFlow(i,lin)) / sumAbsMassFlow(i,lin);
      if (conv < 0.00001)
      {
        hasConverged = true;
      }
      else
      {
        hasConverged = false;
        break;
      }
    }

    iIterations++;
    if (iIterations > 100)
    {
      hasConverged = true;
      cout << endl << "Failed to converge!" << endl;
    
      // reset values to zero
      for (int i=0; i<leakages.size(); i++)
      {
        //leakages[i].P1 = 0.0;
        //leakages[i].P2 = 0.0;
      }
    }
  }*/
}
